#!/bin/sh
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

NAME=$(basename $DIR)

docker build $DIR --tag $NAME