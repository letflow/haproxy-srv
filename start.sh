#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"
NAME=$(basename $DIR)

$DIR/build.sh
docker build $DIR/tests/ --build-arg BASE=$NAME --tag $NAME:test
docker run -p 8400:3306 -p 8401:6379 -p 8500:8500 --env-file $DIR/tests/.env $NAME:test
