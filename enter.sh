#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"
NAME=$(basename $DIR)

DB_PORT=3306
PROXY_STATS_PORT=8500

oneTimeSetUp() {
    $DIR/build.sh
    docker build $DIR/tests/ --build-arg BASE=$NAME --tag $NAME:test
    DB=`docker run --rm --detach \
        --tmpfs /var/lib/mysql:rw \
        -e MYSQL_ALLOW_EMPTY_PASSWORD=yes -e MYSQL_DATABASE=test \
        mariadb:10.3`
    echo "Starting DB container $DB ..."
    DB_HOST=`docker inspect $DB -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}'`
    echo "DB starting at $DB_HOST:$DB_PORT ..."
    $DIR/tests/wait-for $DB_HOST:$DB_PORT -t 60 || exit 1
    echo "DB started successfully"

    PROXY=`docker run --rm --detach \
        -e MYSQL_ADDR=db:$DB_PORT -e STATS_PORT=$PROXY_STATS_PORT \
        --link $DB:db \
        $NAME:test`
    echo "Starting PROXY container $PROXY"
    PROXY_HOST=`docker inspect $PROXY -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}'`
    echo "PROXY starting at $PROXY_HOST:$DB_PORT and stats http://$PROXY_HOST:$PROXY_STATS_PORT ..."
    $DIR/tests/wait-for $PROXY_HOST:$DB_PORT -t 60 || exit 1
    echo "PROXY started successfully"
}

oneTimeTearDown() {
    echo "Stopping container $PROXY and $DB"
    docker stop $PROXY $DB
}

trap oneTimeTearDown EXIT

# init database
oneTimeSetUp

docker exec -it $PROXY sh