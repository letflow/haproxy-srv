FROM haproxy:1.8-alpine

COPY src/*.sh /
COPY src/etc/*.cfg.template /tmp/

ENTRYPOINT ["/docker-entrypoint-srv.sh"]
CMD ["haproxy", "-f", "/usr/local/etc/haproxy/haproxy.cfg"]