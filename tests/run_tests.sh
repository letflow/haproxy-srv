#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"

testMysqlDirect() {
    OUTPUT=`mysql -uroot -h db --silent --batch -e "SELECT 1"`
    assertEquals "Direct connection Cmd" 0 $?
    assertEquals "Direct connection Output" "1" "$OUTPUT"
}

testMysqlProxy() {
    OUTPUT=`mysql -uroot -h 127.0.0.1 --silent --batch -e "SELECT 1"`
    assertEquals "Direct connection Cmd" 0 $?
    assertEquals "Direct connection Output" "1" "$OUTPUT"
}


# Load shUnit2.
. $DIR/shunit2