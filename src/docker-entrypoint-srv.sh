#!/bin/sh

# generate config
./generate-config.sh > /usr/local/etc/haproxy/haproxy.cfg

# call super
exec /docker-entrypoint.sh "$@"
