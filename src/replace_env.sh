#!/bin/sh
FILE=$1
PATTERN='{while(match($0,"[$]{[^}]*}")) {var=substr($0,RSTART+2,RLENGTH -3);gsub("[$]{"var"}",ENVIRON[var])}}1'
if [ "x$FILE" != "x" ]; then
    TEMP=`mktemp`
    cp $FILE $TEMP
    awk "$PATTERN" $TEMP > $FILE 
    rm $TEMP
else
    awk "$PATTERN"
fi