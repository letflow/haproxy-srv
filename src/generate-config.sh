#!/bin/sh

SRC=${1:-/tmp}
TOOLS=$2
export DNS_RESOLVER=${DNS_RESOLVER:-127.0.0.11:53}

${TOOLS}/replace_env.sh < $SRC/haproxy.cfg.template

if [ "x${STATS_PORT}" != "x" ]; then
    ${TOOLS}/replace_env.sh < $SRC/stats.cfg.template
fi

if [ "x${MYSQL_ADDR}" != "x" ]; then
    ${TOOLS}/replace_env.sh < $SRC/mysql.cfg.template
fi

if [ "x${REDIS_ADDR}" != "x" ]; then
    ${TOOLS}/replace_env.sh < $SRC/redis.cfg.template
fi