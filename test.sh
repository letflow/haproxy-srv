#!/bin/bash
DIR="$(cd "$(dirname "$BASH_SOURCE[0]")" && pwd)"
NAME=${1:-$(basename $DIR)}
TEST_NAME="${NAME}-test"

DB_PORT=3306

oneTimeSetUp() {
    docker build $DIR/tests/ --build-arg BASE=$NAME --tag $TEST_NAME
    DB=`docker run --rm --detach \
        --tmpfs /var/lib/mysql:rw \
        -e MYSQL_ALLOW_EMPTY_PASSWORD=yes -e MYSQL_DATABASE=test \
        mariadb:10.3`
    echo "Starting DB container $DB ..."
    DB_HOST=`docker inspect $DB -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}'`
    echo "DB starting at $DB_HOST:$DB_PORT ..."
    $DIR/tests/wait-for $DB_HOST:$DB_PORT -t 60 || exit 1
    echo "DB started successfully"

    PROXY=`docker run --rm --detach \
        -e MYSQL_ADDR=db:$DB_PORT \
        --link $DB:db \
        $TEST_NAME`
    echo "Starting PROXY container $PROXY"
    PROXY_HOST=`docker inspect $PROXY -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}'`
    echo "PROXY starting at $PROXY_HOST:$DB_PORT ..."
    $DIR/tests/wait-for $PROXY_HOST:$DB_PORT -t 60 || exit 1
    echo "PROXY started successfully"
}

oneTimeTearDown() {
    echo "Stopping container $PROXY and $DB"
    docker stop $PROXY $DB
}

trap oneTimeTearDown EXIT

# init database
oneTimeSetUp

docker exec $PROXY /var/opt/tests/run_tests.sh
